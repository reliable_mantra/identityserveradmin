﻿using IdentityServerAdmin.Admin.Configuration.Interfaces;

namespace IdentityServerAdmin.Admin.Configuration
{
    public class AdminConfiguration : IAdminConfiguration
    {
        public string IdentityAdminBaseUrl { get; set; }
        public string IdentityAdminRedirectUri { get; set; }
        public string IdentityServerBaseUrl { get; set; }
        public string ClientId { get; set; }
        public string[] Scopes { get; set; }
        public string ClientSecret { get; set; }
        public string OidcResponseType { get; set; }
    }
}
