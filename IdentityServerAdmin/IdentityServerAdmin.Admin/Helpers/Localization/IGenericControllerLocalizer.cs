﻿using Microsoft.Extensions.Localization;

namespace IdentityServerAdmin.Admin.Helpers.Localization
{
    public interface IGenericControllerLocalizer<T> : IStringLocalizer<T>
    {
        
    }
}