﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using IdentityServerAdmin.Admin.EntityFramework.DbContexts;
using IdentityServerAdmin.Admin.EntityFramework.Identity.Entities.Identity;
using IdentityServerAdmin.Admin.Helpers;

namespace IdentityServerAdmin.Admin
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = BuildWebHost(args);
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                   .UseKestrel(c => c.AddServerHeader = false)
                   .UseStartup<Startup>()
                   .UseSerilog()
                   .Build();
    }
}